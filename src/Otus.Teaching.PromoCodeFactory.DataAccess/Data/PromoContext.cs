﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoContext : DbContext
    {
        public PromoContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Preference> Preferences { get; set; }
        public virtual DbSet<PromoCode> PromoCodes { get; set; }
        public virtual DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            builder.Entity<CustomerPreference>()
                .HasOne<Customer>(sc => sc.Customer)
                .WithMany(s => s.CustomerPreferences)
                .HasForeignKey(sc => sc.CustomerId);


            builder.Entity<CustomerPreference>()
                .HasOne<Preference>(sc => sc.Preference)
                .WithMany(s => s.CustomerPreferences)
                .HasForeignKey(sc => sc.PreferenceId);

            builder.Entity<Role>().HasData(FakeDataFactory.Roles);
            builder.Entity<Employee>().HasData(FakeDataFactory.Employees);

            builder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            builder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            builder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
        }

  
    }

}
