﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Threading.Tasks;

namespace Master.Data;

public interface IDbInitializer
{
    Task InitializeAsync();
}

public class DbInitializer : IDbInitializer
{
    private readonly PromoContext _db;

    public DbInitializer(PromoContext db)
    {
        _db = db;
    }

    public async Task InitializeAsync()
    {
        await _db.Database.MigrateAsync();
    }
}