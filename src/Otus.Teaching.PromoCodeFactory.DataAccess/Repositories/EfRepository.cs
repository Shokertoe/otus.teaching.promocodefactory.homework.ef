﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T>
    : IRepository<T>
    where T : BaseEntity
{
    protected IEnumerable<T> Data { get; set; }
    private readonly PromoContext _context;

    public EfRepository(PromoContext context)
    {
        Data = context.Set<T>();
        _context = context;
    }

    public Task<IEnumerable<T>> GetAllAsync()
    {
        return Task.FromResult(_context.Set<T>() as IEnumerable<T>);
    }

    public Task<T> GetByIdAsync(Guid id)
    {
        return Task.FromResult(_context.Set<T>().FirstOrDefault(x => x.Id == id));
    }

    public async Task<Guid> AddAsync(T entity)
    {
        await _context.AddAsync(entity);
        await _context.SaveChangesAsync();
        return entity.Id;
    }

    public async Task<bool> UpdateAsync(T entity)
    {
        var oldEntity = await _context.Set<T>().FindAsync(entity.Id);
        if (oldEntity is null)
            return false;

        _context.Set<T>().Update(entity);
        await _context.SaveChangesAsync();

        return true;
    }

    public async Task<bool> DeleteAsync(Guid id)
    {
        var set = _context.Set<T>();
        var exist = await set.FindAsync(id);
        if (exist != null)
        {
            set.Remove(exist);
            await _context.SaveChangesAsync();
            return true;
        }

        return false;
    }
}
