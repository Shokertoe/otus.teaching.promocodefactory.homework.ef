﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeResponse
    {
        public Guid Id { get; set; }
        public string PromoCodeData { get; set; }
        public string Date { get; set; }
        public Guid PreferenceId { get; set; }
    }
}
