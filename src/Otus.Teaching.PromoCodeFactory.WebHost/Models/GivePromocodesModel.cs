﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromocodesModel
    {
        public Guid[] PromocodeIds { get; set; }
        public Guid PreferenceId { get; set; }
    }
}
