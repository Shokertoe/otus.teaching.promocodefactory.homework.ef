﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeRequest
    {
        public string PromoCodeData { get; set; }
        public Guid PreferenceId { get; set; }
    }
}
