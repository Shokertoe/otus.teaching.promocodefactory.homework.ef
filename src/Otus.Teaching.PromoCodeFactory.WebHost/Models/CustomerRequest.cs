﻿using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerRequest
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public virtual ICollection<PreferencesRequest> Preferences { get; set; }
        public virtual ICollection<PromoCodeRequest> PromoCodes { get; set; }
    }
}
