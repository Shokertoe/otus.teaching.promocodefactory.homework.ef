﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _repository;

        public PreferencesController(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получить список предочтений
        /// </summary>
        /// <returns>Список предпочтений</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PreferencesResponse>>> GetPreferencesAsync()
        {
            var preferences = await _repository.GetAllAsync();

            var data = preferences.Select(x => new PreferencesResponse()
            {
                Id = x.Id,
                PreferenceDescription = x.Name
            });
            return Ok(data);
        }
    }
}
