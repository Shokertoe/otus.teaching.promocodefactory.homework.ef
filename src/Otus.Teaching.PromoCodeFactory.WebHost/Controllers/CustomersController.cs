﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IRepository<Customer> repo, IRepository<PromoCode> promoCodeRepository)
        {
            _repository = repo;
            _promoCodeRepository = promoCodeRepository;
        }


        /// <summary>
        /// Получить данные о всех заказчиках
        /// </summary>
        /// <returns>Список всех Customers</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerResponse>>> GetCustomersAsync()
        {
            var customers = await _repository.GetAllAsync();
            var result = customers.AsQueryable()
                .Select(x => new CustomerResponse()
                {
                    Preferences = x.CustomerPreferences.Where(c => c.CustomerId == x.Id).Select(cp =>
                            new PreferencesResponse()
                            { Id = cp.PreferenceId, PreferenceDescription = cp.Preference.Name })
                        .ToList(),
                    PromoCodes = x.PromoCodes.Select(pc => new PromoCodeResponse()
                    {
                        PreferenceId = pc.PreferenceId,
                        Id = pc.Id,
                        PromoCodeData = pc.Code,
                        Date = pc.EndDate.ToString(CultureInfo.InvariantCulture)
                    }).ToList(),
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Id = x.Id

                });

            return Ok(result);
        }


        /// <summary>
        ///  Получить данные о заказчике
        /// </summary>
        /// <param name="id">ID заказчика</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var x = await _repository.GetByIdAsync(id);
            return Ok(
                new CustomerResponse()
                {
                    Preferences = x.CustomerPreferences.Where(c => c.CustomerId == x.Id).Select(cp =>
                            new PreferencesResponse()
                                { Id = cp.PreferenceId, PreferenceDescription = cp.Preference.Name })
                        .ToList(),
                    PromoCodes = x.PromoCodes.Select(pc => new PromoCodeResponse()
                    {
                        PreferenceId = pc.PreferenceId,
                        Id = pc.Id,
                        PromoCodeData = pc.Code,
                        Date = pc.EndDate.ToString(CultureInfo.InvariantCulture)
                    }).ToList(),
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Id = x.Id
                });
        }

        /// <summary>
        /// Создать запись о заказчике
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Id новой записи</returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CustomerRequestShort customerRequest)
        {
            var customer = new Customer()
            {
                Email = customerRequest.Email,
                FirstName = customerRequest.FirstName,
                LastName = customerRequest.LastName,
                PromoCodes = new List<PromoCode>()
            };

            var id = await _repository.AddAsync(customer);
          
            return Ok(id);
        }


        /// <summary>
        /// Обновление данных о заказчике
        /// </summary>
        /// <param name="customerRequest">CustomerRequest</param>
        /// <param name="id">Customer Id</param>
        /// <returns>bool</returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateCustomer(Guid id, CustomerRequestShort customerRequest)
        {
            var entity = await _repository.GetByIdAsync(id);
            entity.FirstName = customerRequest.FirstName;
            entity.LastName = customerRequest.LastName;
            entity.Email = customerRequest.Email;
            var result = await _repository.UpdateAsync(entity);
            return Ok(result);
        }

        /// <summary>
        /// Удаление заказчика
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>bool</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var promoCodes = (await  _repository.GetByIdAsync(id)).PromoCodes;

            foreach (var promoCode in promoCodes)
            {
                await _promoCodeRepository.DeleteAsync(promoCode.Id);
            }
            var delete = await _repository.DeleteAsync(id);

            return Ok(delete);
        }
    }
}